import { app } from '../bootstrap/core';
import { clearErrorHook } from '@fino/ui-error-handling/lib';

clearErrorHook(app);
