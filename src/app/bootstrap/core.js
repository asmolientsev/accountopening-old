import angular from 'angular';
import dependencies from './dependencies';

// Import vendor styles (try to use `ui-icon` instead)
import 'font-awesome-sass-loader';

// Create the main angular module called "app".
// It's empty now, but other parts of the app will register things on it.
// Since it is exported, other parts of the application (in other files) can then import it and register things.
export const app = angular.module('app', dependencies);
