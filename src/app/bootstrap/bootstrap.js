/**
 * This file is the main entry point for the entire app.
 *
 * If the application is being bundled, this is where the bundling process
 * starts.  If the application is being loaded by an es6 module loader, this
 * is the entry point.
 *
 * Point Webpack or SystemJS to this file.
 *
 * This module imports all the different parts of the application:
 * - 3rd party Libraries and app core module
 * - Services
 * - Components
 * - Submodules
 * - Top-level states
 * - UI-Router Transition Hooks
 */

// Import the core app module
import { app } from './core';
import { DataModel } from '@fino/lib-datamodel';

// Import Vendors
import _ from 'lodash';

// Import Commons
import '../common/common';

// Import Configs
import '../config/translation/translation';
import '../config/environment/environment';

// Import Styles
import 'normalize.css';
import 'font-awesome-sass-loader';

// Import core ui
import '../common/ui/divider/divider';

// Import the components that make up the main subsections of the application
// Each submodule registers its own states/services/components
import '../app';
import '../components/error/error';
import '../components/accountopening/accountopening';
import '../components/success/success';

// Import any global transition hooks
import '../hooks/clearError';

// Modal declarations
import '../common/ui/modals/agb.tpl.html';
import '../common/ui/modals/infos.tpl.html';
import '../common/ui/modals/schufa.tpl.html';
import '../common/ui/modals/declarations.tpl.html';
import '../common/ui/modals/confirmation.tpl.html';
import '../common/ui/finish/finish.webIdModal.tpl.html';
import '../common/ui/finish/finish.postIdent.tpl.html';

// Introduce Vendors globally to make them injectable for tests
app.constant('_', _);

// Create global data model
app.service('dataModel', DataModel);

//
// RUN
//
app.run(($log, envService) => {
  'ngInject';
  // Print environment to console (TODO dev/testing only)
  $log.debug(angular.toJson(envService.getEnvironment(), true));
});

// Bootstrap the application
angular.bootstrap(document, [app.name])
