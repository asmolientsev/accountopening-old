// Vendor libs
// ------------------------------------

// Angular extensions
import 'angular-filter';
import 'angular-storage';
import 'angular-hotkeys';
import ngMarkdown from 'angular-marked';
import 'angular-dynamic-locale';
import 'spin';
import 'angular-spinner/angular-spinner';
import 'ng-device-detector';

// UI Bootstrap components
import uibModal from 'angular-ui-bootstrap/src/modal';
import uibCollapse from 'angular-ui-bootstrap/src/collapse';
import uibDropdown from 'angular-ui-bootstrap/src/dropdown';

// Angular UI modules
import uiSelect from 'ui-select';
import uiRouter from 'angular-ui-router';
import 'ui-router-extras';

import translate from  'angular-translate';
import translateMessageformat from  'angular-translate-interpolation-messageformat';
import translatePartialLoader from 'angular-translate-loader-partial';
import 'better-dom/dist/better-dom';
import 'better-emmet-plugin/dist/better-emmet-plugin';
import 'better-i18n-plugin/dist/better-i18n-plugin';
import 'better-time-element/dist/better-time-element';
import 'better-dateinput-polyfill/dist/better-dateinput-polyfill';

// fino shared modules
import '@fino/lib-datamodel';
import '@fino/lib-injection';
import ng1Environment from '@fino/ng1-environment';
import uiFormButton from '@fino/ui-form-button';
import uiFormControl from '@fino/ui-form-control';
import uiFormValidation from '@fino/ui-form-validation';
import uiFloatLabel from '@fino/ui-float-label';
import uiInputLabel from '@fino/ui-input-label';
import uiFormFields from '@fino/ui-form-fields';
import uiTranslate from '@fino/ui-translate';
import uiErrorHandling from '@fino/ui-error-handling';
import uiModal from '@fino/ui-modal';

export default [
  'angular.filter',
  'angular-storage',
  'cfp.hotkeys',
  'tmh.dynamicLocale',
  'angularSpinner',
  'ng.deviceDetector',
  uiRouter,
  'ct.ui.router.extras.core',
  ngMarkdown,
  uiSelect,
  uibCollapse,
  uibDropdown,
  uibModal,
  translate,
  translateMessageformat,
  translatePartialLoader,
  ng1Environment.name,
  uiFormButton,
  uiFormControl,
  uiFormValidation,
  uiFloatLabel,
  uiInputLabel,
  uiFormFields,
  uiTranslate,
  uiErrorHandling,
  uiModal
];
