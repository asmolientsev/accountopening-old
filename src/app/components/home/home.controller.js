import { BaseController } from '../../common/base/controller';

export class HomeController extends BaseController {
  constructor(controllerService) {
    'ngInject';
    super({ controllerService });
  }
  
  $onInit() {
    
  }
}
