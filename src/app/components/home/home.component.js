import { HomeController } from './home.controller';
import template from './home.html';
import './home.scss';
import '../../common/ui/hero/hero';

export const homeComponent = {
  name: 'homeComponent',
  restrict: 'E',
  template,
  controller: HomeController,
  controllerAs: 'vm',
  bindings: {}
};
