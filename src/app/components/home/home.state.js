export const homeState = ($stateProvider) => {
  'ngInject';

  $stateProvider.state({
    name: 'home',
    parent: 'app',
    url: '/home',
    template: `<home-component></home-component>`,
    data: { requiresAuth: true }
  });
}
