import { app } from '../../bootstrap/core';
import { successState } from './success.state';
import { successComponent } from './success.component';

app.config(successState);
app.component(successComponent.name, successComponent);
