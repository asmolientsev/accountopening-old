export const successState = ($stateProvider) => {
  'ngInject';

  $stateProvider.state({
    name: 'success',
    parent: 'app',
    url: '/activate/success',
    template: `<success-component></success-component>`,
    data: { requiresAuth: true }
  });
}
