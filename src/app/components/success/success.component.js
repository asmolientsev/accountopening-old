import { SuccessController } from './success.controller';
import template from './success.html';
import './success.scss';

export const successComponent = {
  name: 'successComponent',
  restrict: 'E',
  template,
  controller: SuccessController,
  controllerAs: 'vm',
  bindings: {}
};
