import { BaseController } from '../../common/base/controller';

export class SuccessController extends BaseController {
  constructor(controllerService) {
    'ngInject';
    super({ controllerService });
  }
  
  $onInit() {
    this.$log.log(this)
  }
}
