import { app } from '../../bootstrap/core';
import { accountopeningState, accountopeningStateName } from './accountopening.state';
import { accountopeningComponent } from './accountopening.component';
import { AccountopeningService } from './accountopening.service';
import '../../common/ui/formular/formular';
import '../../common/ui/finish/finish';
import '../../common/ui/creditCard/creditCard';
import '../../common/ui/checkout/checkout';
import '../../common/ui/time/time';

app.config(accountopeningState);
app.constant('accountopeningStateName', accountopeningStateName);
app.service('accountopeningService', AccountopeningService);
app.component(accountopeningComponent.name, accountopeningComponent);
