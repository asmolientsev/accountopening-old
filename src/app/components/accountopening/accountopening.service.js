import { BaseService } from '../../common/base/service';

export class AccountopeningService extends BaseService {
  constructor($http, $httpParamSerializer, $location, dataModel) {
    'ngInject';
    super({ $http, $httpParamSerializer, $location, dataModel });
  }

  previousState(data) {
    return this.$http({
      method: 'POST',
      url: '/api/v0/previousState',
      data: data
    }).then(resp => {
      const state = resp.data;
      const user = this.getUser();
      const stateName = this.findStateNameById(user.states, state.id);
      return stateName;
    });
  }

  nextState(data) {
    return this.$http({
      method: 'POST',
      url: '/api/v0/nextState',
      data: data,
      error: {
        status: [409],
        message: 'app.accountopening.error'
      }
    }).then(resp => {
      const state = resp.data;
      const stateName = this.findStateNameById(data.states, state.id);

      if (this.isLastState(data.states, state.id)) {
        return { isLastState: true };
      }

      this.saveToViewState(stateName, 'state', state);
      return { stateName };
    });
  }

  startNewProcess() {
    let user = this.newUser();
    let queryString = this.$httpParamSerializer(user);
    let url = `/api/v0/startNewProcess?${queryString}`;
    return this.$http({
      method: 'GET',
      url
    }).then(resp => {
      const newProcess = resp.data;
      user.treatyId = newProcess.treatyId;
      user.states = newProcess.states;
      user.visibleStates = this.getStateKeys(_.takeRight(user.states, _.size(user.states) - 1));
      this.setUser(user);
      return user;
    });
  }

  startWebIdProcess() {
    const { tenant, treatyId } = this.getUser();
    const { $http } = this;

    /*const queryString = this.$httpParamSerializer({ tenant, treatyId });
    const url = `${$location.protocol()}://${$location.host()}:${$location.port()}/api/v0/startWebIdProcess?${queryString}`;
    $window.location.href = url;*/

    return $http({
      method: 'GET',
      url: `/api/v0/startWebIdProcess`,
      params: {
        tenant,
        treatyId
      }
    }).then(resp => resp.data);
  }

  clear() {
    this.dataModel.remove('user');
    this.dataModel.remove('viewStates');
  }

  getUser() {
    return this.dataModel.get('user');
  }

  setUser(user) {
    this.dataModel.set('user', user);
  }

  saveToUser(data) {
    let user = this.dataModel.get('user') || {};
    this.setUser(_.merge({}, user, data));
  }

  newUser() {
    //TODO Do not hard code tenant
    return {
      tenant: 'KT-Bank'
    };
  }

  getViewState(stateName) {
    const viewStates = this.dataModel.get('viewStates');
    return viewStates && viewStates[stateName];
  }

  setViewState(stateName, viewState) {
    let viewStates = this.dataModel.get('viewStates') || {};
    viewStates[stateName] = viewState;
    this.dataModel.set('viewStates', viewStates);
  }

  saveUserData(stateName, userData) {
    this.saveToViewState(stateName, 'userData', userData);
  }

  saveToViewState(stateName, key, value) {
    const viewState = this.getViewState(stateName) || {};
    viewState[key] = value;
    this.setViewState(stateName, viewState);
  }

  isLastState(states, state) {
    const last = _.last(states);
    return state && last && _.includes(last.stateIds, state);
  }

  isFirstState(states, state) {
    const first = _.first(states);
    return state && first && _.includes(first.stateIds, state);
  }

  findStateNameById(states, stateId) {
    const state = _.find(states, (value) => {
      return _.includes(value.stateIds, stateId);
    });
    return state && state.stateName;
  }

  getStateKeys(states) {
    return _.map(states, s => s.stateName);
  }
}
