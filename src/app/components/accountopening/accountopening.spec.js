import AccountopeningModule from './accountopening'
import AccountopeningController from './accountopening.controller';
import AccountopeningComponent from './accountopening.component';
import AccountopeningTemplate from './accountopening.html';

describe('Accountopening', () => {
  let $rootScope, makeController;

  beforeEach(window.module(AccountopeningModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new AccountopeningController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(AccountopeningTemplate).to.match(/{{\s?vm\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = AccountopeningComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(AccountopeningTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(AccountopeningController);
      });
  });
});
