import { AccountopeningController } from './accountopening.controller';
import template from './accountopening.html';
import './accountopening.scss';

export const accountopeningComponent = {
  name: 'accountopeningComponent',
  restrict: 'E',
  template,
  controller: AccountopeningController,
  controllerAs: 'vm',
  bindings: {}
};
