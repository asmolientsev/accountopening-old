export const accountopeningStateName = 'accountopening';

export const accountopeningState = ($stateProvider) => {
  'ngInject';

  $stateProvider.state({
    name: accountopeningStateName,
    parent: 'app',
    url: '/:stateName',
    template: '<accountopening-component></accountopening-component>',
    data: { requiresAuth: true },
    resolve: {
      syncState
    }
  });
}

const syncState = (accountopeningService, $stateParams) => {
  'ngInject';

  const stateName = $stateParams && $stateParams.stateName;
  const user = accountopeningService.getUser();
  const stateDiffernce =
    stateName
    && user
    && user.visibleStates
    && user.visibleStates.length
    && user.visibleStates.indexOf(stateName) - user.visibleStates.indexOf(user.currentStateId);

  // if current state synced with backend, do nothing
  if (!angular.isDefined(stateDiffernce) || stateDiffernce === 0) {
    return;
  }

  // if current state index higher than state in backend, post next state to sync with backend
  if (stateDiffernce < 4 && stateDiffernce > 0) {
    const viewState = accountopeningService.getViewState(user.currentStateId);
    const data = _.merge({}, user, { payloads: viewState && viewState.userData });
    accountopeningService.saveToUser({ currentStateId : stateName });
    return accountopeningService.nextState(data);
  }

  // ... otherwise post previous state
  if (stateDiffernce < 0) {
    accountopeningService.saveToUser({ currentStateId : stateName });
    return accountopeningService.previousState(user);
  }
}
