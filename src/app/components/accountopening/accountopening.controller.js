import { BaseController } from '../../common/base/controller';
import mockData from './mock';

const FinishState = 'activate';

export class AccountopeningController extends BaseController {
  constructor(
    $scope,
    $rootScope,
    $location,
    controllerService,
    accountopeningService,
    accountopeningStateName,
    $q,
    tmhDynamicLocale,
    $stateParams,
    uiModalDeclarationService) {
    'ngInject';
    super({
      $scope,
      $rootScope,
      $location,
      controllerService,
      accountopeningService,
      accountopeningStateName,
      $q,
      tmhDynamicLocale,
      uiModalDeclarationService});

    this.stateName = $stateParams.stateName;
    if (this.stateName === FinishState) {
      this.hasFinished = true;
      this.stateHeader = 'accountopening.finished';

      this.backButtonConfirmInit($location);
    }
  }
 
  get payload() {
    return angular.equals({}, this.model) ? { payloads: { } } : { payloads: this.model };
  }

  get header() {
    return this.stateHeader || `app.header.${this.stateName}`;
  }

  get isFirstState() {
    return this.user && this.accountopeningService.isFirstState(this.user.states, this.stateName);
  }

  get showCheckout() {
    return this.user && _.includes(this.user.visibleStates, this.stateName);
  }

  get userData() {
    return _.merge(
      {},
      this.user || {},
      { currentStateId: this.stateName },
      { language: this.$translate.use() }
    );
  }

  get stateAsClassName() {
    let className = _.kebabCase(this.stateName);
    if (this.isFirstState) {
      className += ' first-state';
    }
    return className;
  }

  get isReady() {
    return this.viewReady && !this.isLoading;
  }

  $onInit() {
    const { tmhDynamicLocale, $translate, $scope } = this;

    tmhDynamicLocale
      .set($translate.use())
      .finally(this.activate.bind(this));
  }

  activate() {
    this.bindToLanguageChange(this.$scope);
    this.user = this.accountopeningService.getUser();
    this.loading(false);
    this.viewReady = false;
    if (this.user && this.stateName) {
      if (this.hasFinished) {
        this.stateName = _.last(this.user.visibleStates);
        return this.viewReady = true;
      }

      const viewState = this.accountopeningService.getViewState(this.stateName);
      if (viewState) {
        return this.updateView(viewState);
      }
    }
    this.startNew();
  }

  loading(loading) {
    this.isLoading = loading;
  }

  formInitialized(formController) {
    this.formController.push(formController);
  }

  startNew() {
    this.user = null;
    this.accountopeningService.clear();
    this.accountopeningService.startNewProcess().then(user => {
      this.requestNext(user);
    });
  }

  restart() {
    this.go(this.accountopeningStateName, { stateName:'' });
  }

  requestNext(user) {
    let data = _.merge({}, user || this.userData, this.payload );

    this.accountopeningService.nextState(data).then((result) => {
      if (result.isLastState) {
        return this.navigate(FinishState);
      }
      this.navigate(result.stateName);
    })
    .finally(() => {
      this.loading(false);
    });
  }

  requestPrevious() {
    this.accountopeningService.previousState(this.userData).then(stateName => {
      this.navigate(stateName);

      // TODO get model from server, currently in local storage
      //this.model = last.payloads;
    });
  }

  updateView(viewState) {
    const { state, userData } = viewState;

    this.stateHeader = `app.header.${state.header}`;
    this.customers = _.map(state.fieldGroups, fg => { return { id: fg.fieldGroupId, fieldGroups: [fg] } });
    this.formController = [];
    this.loadUserData(userData);
    this.backNavigationAllowed = state.backNavigationAllowed;
    this.viewReady = true;
  }

  loadUserData(userData) {
    if (userData) {
      return this.model = userData;
    }
    this.model = _.reduce(this.customers, (r, c) => {
      r[c.id] = {};
      return r;
    }, {});
  }

  submit() {
    let validators = _.map(this.formController, fc => fc.validate());
    this.$q.all(validators).then(() => {
      this.accountopeningService.saveUserData(this.stateName, this.model);
      this.loading(true);
      this.requestNext();
    });
  }

  fillRandomData() {
    _.forEach(this.formController, fc => fc.fillRandomData());
  }

  navigate(stateName) {
    this.accountopeningService.saveToUser({ currentStateId : stateName });
    this.go(this.accountopeningStateName, { stateName });
  }

  backButtonConfirmInit($location) {
    this.$rootScope.$on('$locationChangeStart', (event, newLocation) => {
      let searchStr = '/acceptAccountOpening';
      if (_.includes(newLocation, searchStr)) {
        this.backButtonConfirmModal();
        event.preventDefault();
      }
    });
  }

  backButtonConfirmModal() {
    const {$scope} = this;
    $scope.confirmTitle = 'accountopening.modal.confirmation.title';
    $scope.confirmBody = 'accountopening.modal.confirmation.body';
    $scope.confirmOk = 'accountopening.modal.confirmation.confirm';
    $scope.confirmDismiss = 'accountopening.modal.confirmation.dismiss';
    $scope.showDismissBtn = true;
    $scope.confirmModal = this.uiModalDeclarationService.openUrl({
      url: 'app/common/ui/modals/confirmation.tpl.html',
      options: {
        scope: $scope
      }
    });
  }

  backButtonConfirmRedirect($modal) {
    $modal.close();
    this.restart();
  }
}
