export default {
  "id": "acceptAccountOpening",
  "header": "acceptAccountOpening",
  "fieldGroups": [
    {
      "title": null,
      "subtitle": null,
      "descriptionHeaderKey": null,
      "descriptionBodyKey": null,
      "fieldDefinitions": [
        {
          "type": "checkbox",
          "name": "agbs",
          "options": {
            "type": "checkbox",
            "selectables": [{
              "key": "agbs",
              "value": "agbs",
              "conflicts": []
            }]
          },
          "gridCol": 12,
          "required": true
        },
        {
          "type": "checkbox",
          "name": "schufa",
          "options": {
            "type": "checkbox",
            "selectables": [{
              "key": "schufa",
              "value": "schufa",
              "conflicts": []
            }]
          },
          "gridCol": 12,
          "required": true
        },
        {
          "type": "checkbox",
          "name": "readInfos",
          "options": {
            "type": "checkbox",
            "selectables": [{
              "key": "schufa",
              "value": "schufa",
              "conflicts": []
            }]
          },
          "gridCol": 12,
          "required": true
        },
        {
          "type": "select-one",
          "name": "ownEconomicInterest",
          "options": {
            "type": "select-one",
            "selectables": [
              {
                "key": "yes",
                "value": "yes",
                "conflicts": []
              },
              {
                "key": "noOwnEconomicInterest",
                "value": "noOwnEconomicInterest",
                "conflicts": []
              }
            ]
          },
          "gridCol": 12,
          "required": true
        }
      ]
    }
  ]
}
