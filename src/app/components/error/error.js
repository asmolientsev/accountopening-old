import { app } from '../../bootstrap/core';
import { errorState } from './error.state';
import { errorComponent } from './error.component';

app.config(errorState);
app.component(errorComponent.name, errorComponent);
