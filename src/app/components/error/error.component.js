import { ErrorController } from './error.controller';
import template from './error.html';
import './error.scss';

export const errorComponent = {
  name: 'errorComponent',
  restrict: 'E',
  template,
  controller: ErrorController,
  controllerAs: 'vm',
  bindings: {}
};
