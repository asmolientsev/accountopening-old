export const errorState = ($stateProvider) => {
  'ngInject';

  $stateProvider.state({
    name: 'error',
    parent: 'app',
    url: '/error',
    template: `<error-component></error-component>`,
    data: { requiresAuth: true }
  });
}
