import { BaseController } from '../../common/base/controller';

const ErrorWhiteList = ['email'];

export class ErrorController extends BaseController {
  constructor(controllerService) {
    'ngInject';
    super({ controllerService });
  }

  $onInit() {
    this.displayError = _.pick(this.error, ErrorWhiteList);
  }
}
