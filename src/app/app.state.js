import { appComponent } from './app.component';
import './app.scss';
import _ from 'lodash';

export const appState = ($stateProvider) => {
  'ngInject';
  
  // Converts component name to kebab case.
  const componentName = _.kebabCase(appComponent.name);

  $stateProvider.state({
    abstract: true,
    name: 'app',
    redirectTo: 'home',
    template: `<${componentName}></${componentName}>`
  });
}
