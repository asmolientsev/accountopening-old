import { app } from './bootstrap/core';
import { appState } from './app.state';
import { appComponent } from './app.component';
import './common/ui/appHeader/appHeader';

app.config(appState);
app.component('app', appComponent);

// Apply some global configuration...

// If the user enters a URL that doesn't match any known URL (state),
// send them to `/` by default.
app.config(($locationProvider, $urlRouterProvider, $httpProvider, $sceDelegateProvider, fdFormFieldsProvider, errorInterceptorProvider) => {
  'ngInject';
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $httpProvider.interceptors.push('errorInterceptor');

  fdFormFieldsProvider.setTypeOptions(fdFormFieldsProvider.types.CHECKBOX, { combinedModel: true });
  fdFormFieldsProvider.setTypeOptions(fdFormFieldsProvider.types.SELECT, {
    modifier: {
      appendix: 'Translated',
      modify: ($translate) => {
        'ngInject';

        return (value) => {
          if (value) {
            return $translate.instant(`accountopening.form.${value}`);
          }
        }
      }
    }
  });
  fdFormFieldsProvider.setTypeOptions(fdFormFieldsProvider.types.COUNTRY_SELECT, { priorities: ['DE', 'TR', 'KW', 'FR', 'GB'] });
  errorInterceptorProvider.setDefaultErrorMessage('app.accountopening.error');

  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow loading from our assets domain.
    'http://cdn.accountopening.fino.digital/**',
    'https://cdn.accountopening.fino.digital/**',
    'self'
  ]);
});

