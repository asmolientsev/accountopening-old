import { app } from '../../../bootstrap/core';
import { formularComponent } from './formular.component';
import { transcludeStates, centerStates } from './formular.constants';

app.component(formularComponent.name, formularComponent);
app.constant('transcludeStates', transcludeStates);
app.constant('centerStates', centerStates);
