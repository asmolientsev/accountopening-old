import { BaseController } from '../../../common/base/controller';
import IBAN from 'iban';

export class FormularController extends BaseController {
  constructor($scope, $timeout, $transclude, controllerService, transcludeStates, centerStates, fdFormFieldsService, hotkeys) {
    'ngInject';
    super({ $scope, $timeout, $transclude, controllerService, transcludeStates, centerStates, fdFormFieldsService, hotkeys });

  }

  get center() {
    return this.state && _.includes(this.centerStates, this.state);
  }

  get transclude() {
    return this.state && _.includes(this.transcludeStates, this.state);
  }

  get form() {
    return this[this.formId];
  }

  get formController() {
    return {
      validate: () => {
        return this.fdFormFieldsService
          .validate(this.form);
      },
      fillRandomData: this.fillRandomData.bind(this),
      reset: this.reset.bind(this),
      toggleDevMode: this.toggleDevMode.bind(this)
    };
  }

  $onInit() {
    const { $scope, hotkeys } = this;

    this.bindToLanguageChange($scope);

    if (this.controlFromAbove) {
      const { formController } = this;
      return this.initialized({ formController });
    }

    hotkeys.bindTo($scope)
      .add({
        combo: 'r',
        description: 'Fill form with random data',
        callback: () => this.fillRandomData()
      });

    hotkeys.bindTo($scope)
      .add({
        combo: 'ctrl+l',
        description: 'Toggle dev mode',
        callback: () => this.toggleDevMode()
      });
  }

  save() {
    return this.fdFormFieldsService
      .validate(this.form)
      .then(() => {
        this.next()
      });
  }

  reset() {
    this.fdFormFieldsService.resetFields(this.form);
  }

  fillRandomData() {
    if (!this.envService.isProduction()) {
      this.fdFormFieldsService.fillRandomData(this.form);
    }
  }

  toggleDevMode() {
    this.fdFormFieldsService.toggleDevMode(this.form);
  }
}
