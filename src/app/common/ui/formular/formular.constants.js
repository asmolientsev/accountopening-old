'use strict';

export const transcludeStates = ['personalInformation'];

export const transclude = _.reduce(transcludeStates, (acc, val) => {
  acc[val] = val;
  return acc;
}, {});

export const centerStates = ['selectAccountType', 'configAccount'];
