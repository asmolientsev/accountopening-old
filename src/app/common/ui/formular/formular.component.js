import { FormularController } from './formular.controller';
import { transclude } from './formular.constants';
import template from './formular.html';
import './formular.scss';

export const formularComponent = {
  name: 'formularComponent',
  restrict: 'E',
  transclude,
  template,
  controller: FormularController,
  controllerAs: 'vm',
  bindings: {
    formId: '@',
    state: '@',
    model: '=',
    fieldGroups: '<',
    controlFromAbove: '<',
    initialized: '&',
    next: '&',
    backNavigationAllowed: '<',
    back: '&'
  }
};
