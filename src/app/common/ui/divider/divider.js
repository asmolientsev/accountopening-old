import { app } from '../../../bootstrap/core';
import dividerComponent from './divider.component';

app.component('divider', dividerComponent);
