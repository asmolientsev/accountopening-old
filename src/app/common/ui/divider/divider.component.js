import template from './divider.html';
import './divider.scss';

export default {
  restrict: 'E',
  transclude: {
    left: 'left',
    right: 'right'
  },
  controllerAs: 'vm',
  bindings: {
    vertical: '<'
  },
  template
};
