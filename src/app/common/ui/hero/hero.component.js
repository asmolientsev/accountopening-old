import template from './hero.html';
import './hero.scss';

export default {
  restrict: 'E',
  bindings: {
    title: '@',
    lead: '@'
  },
  template
};
