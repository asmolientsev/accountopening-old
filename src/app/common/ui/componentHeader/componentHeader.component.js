import template from './componentHeader.html';
import './componentHeader.scss';

const componentHeader = {
  restrict: 'E',
  bindings: {
    title: '<',
    body: '<'
  },
  template
};

export default componentHeader;
