import { app } from '../../../bootstrap/core';
import { finishComponent } from './finish.component';

app.component(finishComponent.name, finishComponent);
