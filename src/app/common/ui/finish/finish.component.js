import { FinishController } from './finish.controller';
import template from './finish.html';
import './finish.scss';

export const finishComponent = {
  name: 'finishComponent',
  restrict: 'E',
  template,
  controller: FinishController,
  controllerAs: 'vm',
  bindings: {
    user: '<',
    loading: '&'
  }
};
