import { BaseController } from '../../../common/base/controller';

export class FinishController extends BaseController {
  constructor(controllerService, accountopeningService) {
    'ngInject';
    super({ controllerService, accountopeningService });

    this.agb = false;
  }

  get startWebIdProcessLink() {
    return this.accountopeningService.startWebIdProcessLink();
  }

  $onInit() {
    this.$log.log(this)
  }

  goToSuccessState() {
    this.go('success'); 
  }

  submitAgbForm($modal) {
    if (this.webIdAgbForm.$valid && this.agb) {
      const { $window } = this;

      this.loading({ loading: true });
      $modal.close();
      this.accountopeningService
        .startWebIdProcess()
        .then(data => {
          if (data && data.result && data.msg) {
            return $window.location.href = data.msg.url;
          }
          this.errored(data.msg);
        });
    }
  }
}
