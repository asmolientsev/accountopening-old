import template from './creditCard.html';
import './creditCard.scss';

export const creditCardComponent = {
  name: 'creditCardComponent',
  restrict: 'E',
  template,
  controllerAs: 'vm',
  bindings: {
    model: '='
  }
};
