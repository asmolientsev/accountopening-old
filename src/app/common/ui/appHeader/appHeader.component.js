import { AppHeaderController } from './appHeader.controller';
import template from './appHeader.html';
import './appHeader.scss';

export default {
  restrict: 'E',
  controller: AppHeaderController,
  controllerAs: 'vm',
  bindings: {},
  template
};
