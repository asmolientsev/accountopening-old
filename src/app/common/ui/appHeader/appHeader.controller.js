import { BaseController } from '../../../common/base/controller';

const LANG_CODE_TO_FLAG = {
  'en': 'gb'
};

export class AppHeaderController extends BaseController {
  constructor(controllerService, $rootScope, supportedLanguages, tmhDynamicLocale) {
    'ngInject';
    super({ controllerService, $rootScope, supportedLanguages, tmhDynamicLocale });
  }

  $onInit() {
    const usedLanguage = this.$translate.use();
    this.setSelectables();
    this.selectedLanguage = _.find(this.selectables, s => s.key === usedLanguage);

    this.tmhDynamicLocale.set(usedLanguage);
    this.$rootScope.$broadcast('languageChanged');
  }

  setSelectables() {
    this.selectables = _.map(this.supportedLanguages, lang => {
      return {
        key: lang,
        flagKey: LANG_CODE_TO_FLAG[lang] || lang
      };
    });
  }

  useLanguage(lang) {
    this.$translate.use(lang);
    this.setSelectables();
    this.tmhDynamicLocale.set(lang).finally(() => {
      this.$rootScope.$broadcast('languageChanged');
    });
  }
}
