import { app } from '../../../bootstrap/core';
import appHeaderComponent from './appHeader.component';
import { supportedLanguages } from './appHeader.constants';
import '../navbar/navbar';

app.component('appHeader', appHeaderComponent);
app.value('supportedLanguages', supportedLanguages);
