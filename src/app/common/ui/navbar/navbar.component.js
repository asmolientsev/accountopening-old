import template from './navbar.html';
import controller from './navbar.controller';
import './navbar.scss';

export const navbarComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
};
