import { BaseController } from '../../../common/base/controller';

class NavbarController extends BaseController {
  constructor(controllerService, navbarService) {
    'ngInject';
    super({ controllerService, navbarService });
  }

  $onInit() {
  const { navbarService } = this;
    this.navbarTabs = navbarService.getTabs();
  }
}

export default NavbarController;
