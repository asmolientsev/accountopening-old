import template from './time.html';

export const timeComponent = {
  name: 'time',
  restrict: 'E',
  template,
  controllerAs: 'vm',
  bindings: {
    time: '<'
  }
};
