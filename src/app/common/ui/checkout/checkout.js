import { app } from '../../../bootstrap/core';
import { checkoutComponent } from './checkout.component';

app.component(checkoutComponent.name, checkoutComponent);
