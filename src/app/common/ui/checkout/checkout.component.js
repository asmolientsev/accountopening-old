import template from './checkout.html';
import './checkout.scss';

export const checkoutComponent = {
  name: 'checkout',
  restrict: 'E',
  template,
  controllerAs: 'vm',
  bindings: {
    states: '<',
    currentState: '@'
  }
};
