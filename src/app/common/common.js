import { app } from '../bootstrap/core';
import { ControllerService } from './base/controller.service';

app.service('controllerService', ControllerService);
