import { BaseService } from './service';

export class ControllerService extends BaseService {
  constructor(
    $log,
    $translate,
    $locale,
    $state,
    $stateParams,
    $window,
    dataModel,
    envService,
    errorService
  ) {
    'ngInject';
    super({
      $log,
      $translate,
      $locale,
      $state,
      $stateParams,
      $window,
      dataModel,
      envService,
      errorService
    });
  }
}
