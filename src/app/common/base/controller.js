/* global _:false */

export class BaseController {
  constructor(injections, options) {
    // Assign given injections to the controllers instance
    this.constructor.injectArguments(this, injections, options);

    // Ensure that the mandatory controllerSevice has been injected
    this.constructor.contractInjection(this, 'controllerService');

    this.unwatchers = [];
    this.languageChanged();
  }

  get $log() {
    return this.controllerService.$log;
  }

  get $locale() {
    return this.controllerService.$locale;
  }

  get $translate() {
    return this.controllerService.$translate;
  }

  get $state() {
    return this.controllerService.$state;
  }

  get $stateParams() {
    return this.controllerService.$stateParams;
  }

  get $window() {
    return this.controllerService.$window;
  }

  get authService() {
    return this.controllerService.authService;
  }

  get dataModel() {
    return this.controllerService.dataModel;
  }

  get envService() {
    return this.controllerService.envService;
  }

  get error() {
    return this.dataModel.get('error');
  }

  errored(error) {
    this.dataModel.set('error', error);
    this.go('error');
  }

  go(to, params = {}, options = {}) {
    //debugger;
    this.constructor.contractInjection(this, '$state');

    // Ensure that the given parameters are splitted
    // to the target state and its parameters. If the
    // given to parameter contains a state object, we
    // have to deconstruct this object first.
    if (to.state) {
      params = _.merge(to.params || {}, params);
      to = to.state.name;
    }

    if (options.silent) {
      return this.$state.go(to, params, { location: 'replace' });
    }
    return this.$state.go(to, params);
  }

  goBack() {
    this.constructor.contractInjection(this, '$window');
    this.$window.history.back();

    // Support native app history
    if (navigator && navigator.app) {
      navigator.app.backHistory();
    }
  }

  reload(params) {
    this.go(this.$state.current, params);
  }

  bindToLanguageChange(scope) {
    this.unwatchers.push(scope.$on('languageChanged', () => this.languageChanged()));
  }

  $destroy() {
    _.forEach(this.unwatchers, uw => uw());
  }

  languageChanged() {
    let currentLang = this.$translate.use();
    let dateFormat = (currentLang == 'de') ? this.$locale.DATETIME_FORMATS.mediumDate:this.$locale.DATETIME_FORMATS.shortDate;

    this.languageOptions = {
        lang: currentLang,
        dateFormat: dateFormat
    };
    this.documentLanguage =
      _.includes(this.envService.envConfig.documentLanguages, this.languageOptions.lang) ?
        this.languageOptions.lang :
        'en';
  }
}
