'use strict';

export class BaseClass {
  constructor(injections, options) {
    // Assign given injections to the classes instance
    this.constructor.injectArguments(this, injections, options);
  }
}
