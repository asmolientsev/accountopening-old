import { app } from '../../bootstrap/core';

// Locale
import 'angular-i18n/angular-locale_de';
import 'angular-i18n/angular-locale_en';
import 'angular-i18n/angular-locale_tr';

const DefaultLanguage = 'de';

app.constant('DefaultLanguage', DefaultLanguage);

app.config(function ($translateProvider, $translatePartialLoaderProvider, tmhDynamicLocaleProvider) {
  'ngInject';

  // Escaping of variable content
  $translateProvider.useLoaderCache(true);
  $translateProvider.useSanitizeValueStrategy('escaped');
  $translateProvider.useMessageFormatInterpolation();

  // Use partial loader for translations loading
  $translateProvider.useLoader('$translatePartialLoader', {
    urlTemplate: '{part}/lang/{lang}.json'
  });

  // Add initial translate partial(s)
  $translatePartialLoaderProvider.addPart('app');

  // Set and use default language
  $translateProvider.preferredLanguage(DefaultLanguage);
  $translateProvider.use(DefaultLanguage);

  tmhDynamicLocaleProvider.localeLocationPattern('angular-locale_{{locale}}.js');
});

app.run(function ($rootScope, $translate) {
  'ngInject';
  /*eslint-disable angular/on-watch, no-unused-vars*/
  $rootScope.$on('$translatePartialLoaderStructureChanged', () => {
    $translate.refresh();
  });
  /*eslint-enable angular/on-watch, no-unused-vars*/
});
