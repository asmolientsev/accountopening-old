import { app } from '../../bootstrap/core';

const prodBlock = function (envService, supportedLanguages) {
  'ngInject';
  if (envService.isProduction()) {
    app.value('supportedLanguages', supportedLanguages);
  }
};

app.run(prodBlock);
