# Attention to this folder

## Please read this before start editing the files inside this `modules` folder.

The folder contains the two files `modules.js` and `modules.scss` and the folder `export`.
Each of those two files contains an inject-section which is being used by the gulp modules task `gulp modules`
to automatically inject all modules that are defined as dependencies inside the `package.json`.

In other words: As long as you only want to ensure that all custom dependencies from your package.json
(prefixed with `@fino`) are injected and provided to the app, *DO NOTHING*!
