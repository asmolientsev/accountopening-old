#!/usr/bin/env bash

set -e

docker push dockerhub.fino.digital/fino/accountopening-ui:${BUILD_NUMBER}
