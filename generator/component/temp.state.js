export const <%= name %>State = ($stateProvider) => {
  'ngInject';

  $stateProvider.state({
    name: 'app.<%= name %>',
    parent: 'app',
    url: '/<%= name %>',
    template: `<<%= kebabName %>-component></<%= kebabName %>-component>`,
    data: { requiresAuth: true }
  });
}
