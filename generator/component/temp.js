import { app } from '../../bootstrap/core';
import { <%= name %>State } from './<%= name %>.state';
import { <%= name %>Component } from './<%= name %>.component';

app.config(<%= name %>State);
app.component(<%= name %>Component.name, <%= name %>Component);
