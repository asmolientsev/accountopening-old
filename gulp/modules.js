import gulp from 'gulp';
import inject from 'gulp-inject';
import rename from 'gulp-rename';
import _ from 'lodash';
import conf from './conf';
import { camelCased } from './helper';

gulp.task('modules', ['clean:modules', 'modules:scss']);

/**
 * Gether and inject the styles of all modules
 * that are defined as dependencies into the modules.scss.
 */
gulp.task('modules:scss', () => {
  const deps = conf.dependencies;
  const depFiles = [];
  // Collect the style property of each dependency
  _.forEach(deps, d => d.style && depFiles.push(d.style));
  const files = gulp.src(depFiles, { read: false });
  
  return gulp.src(conf.pathToApp('modules/modules.scss'))
    .pipe(inject(files, {
      starttag: '// inject:modules.{{ext}}',
      endtag: '// endinject:modules.{{ext}}',
      transform: (filepath) => {
        filepath = filepath.replace(/\/node_modules\//, '~');
        return `@import '${filepath}';`;
      }
    }))
    .pipe(rename('modules.export.scss'))
    .pipe(gulp.dest(conf.pathToApp('modules')));
});
