import gulp from 'gulp';
import gutil from 'gulp-util';
import colorsSupported from 'supports-color';
import conf from './conf';
import webpack from 'webpack';
import webpackConfig from '@fino/ng1-webpack';

// use webpack.config.js to build modules
gulp.task('build', ['clean', 'env', 'modules'], (cb) => {
  const config = webpackConfig.dist(conf);
  config.entry.app = conf.paths.entry;

  webpack(config, (err, stats) => {
    if(err)  {
      throw new gutil.PluginError('webpack', err);
    }

    gutil.log('[webpack]', stats.toString({
      colors: colorsSupported,
      chunks: false,
      errorDetails: true
    }));

    cb();
  });
});
