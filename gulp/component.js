import gulp from 'gulp';
import template from 'gulp-template';
import rename from 'gulp-rename';
import { argv } from 'yargs';
import path from 'path';
import _ from 'lodash';
import conf from './conf';
import { upperCamel, lowerCamel } from './helper';

gulp.task('component', () => {
  const name = argv.name;
  const parentPath = argv.parent || '';
  const destPath = path.join(conf.pathToComponents(), parentPath, name);

  return gulp.src(conf.paths.blankTemplates)
    .pipe(template({
      name: name,
      upperCaseName: upperCamel(name),
      lowerCaseName: lowerCamel(name),
      kebabName: _.kebabCase(name)
    }))
    .pipe(rename((file) => {
      file.basename = file.basename.replace('temp', name);
    }))
    .pipe(gulp.dest(destPath));
});
