/* global require,process */
'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');

var ENV = {
  PRODUCTION  : 'production',
  DEVELOPMENT : 'development',
  TESTING     : 'testing',
  LOCAL       : 'local'
};

function setEnvironment(env) {

  process.env.api_port = process.env.api_port || '';
  process.env.api_host = process.env.api_host || '';

  switch (env) {
    case ENV.PRODUCTION:
      process.env.NODE_ENV = ENV.PRODUCTION;
      process.env.api_port = process.env.api_port || 443;
      process.env.api_host = process.env.api_host || 'https://kontoeroeffnung.fino.digital';
    break;

    case ENV.TESTING:
      process.env.NODE_ENV = ENV.TESTING;
      process.env.api_port = process.env.api_port || 443;
      process.env.api_host = process.env.api_host || 'https://kontoeroeffnung-testing.fino.digital';
    break;

    case ENV.DEVELOPMENT:
      process.env.NODE_ENV = ENV.DEVELOPMENT;
      process.env.api_port = process.env.api_port || 443;
      process.env.api_host = process.env.api_host || 'https://kontoeroeffnung-dev.fino.digital';
    break;

    case ENV.LOCAL:
      process.env.NODE_ENV = ENV.LOCAL;
      process.env.api_port = process.env.api_port || 8080;
      process.env.api_host = process.env.api_host || 'http://localhost';
    break;
  }

  gutil.log('Set environment to', gutil.colors.cyan(process.env.NODE_ENV));
  gutil.log('Set API PORT to', gutil.colors.cyan(process.env.api_port));
  gutil.log('Set API HOST to', gutil.colors.cyan(process.env.api_host));
}

gulp.task('env', function() {
  // Default environment is 'production'
  var environment = ENV.DEVELOPMENT;
  if (gutil.env.production) {
    environment = ENV.PRODUCTION;
  }
  else if (gutil.env.testing) {
    environment = ENV.TESTING;
  }
  else if (gutil.env.local) {
    environment = ENV.LOCAL;
  }
  setEnvironment(environment);
});

gulp.task('env:prod', function() {
  setEnvironment(ENV.PRODUCTION);
});

gulp.task('env:dev', function() {
  setEnvironment(ENV.DEVELOPMENT);
});

gulp.task('env:testing', function() {
  setEnvironment(ENV.TESTING);
});

gulp.task('env:local', function() {
  setEnvironment(ENV.LOCAL);
});
