import path from 'path';
import { getDependencies, readJson } from './helper';

/**
 *  This variables defines the root path of the application.
 */
const root = '../src';

/**
 *  This variable contains the name of your application's deployment environment.
 *  Common values include 'dev', 'testing', 'production'
 */
const env = process.env.NODE_ENV;

/**
 *  Read the used port from the current process environment.
 *  Defaults to 3000.
 */
const port = process.env.PORT || 3000;

/**
 *  Contains all dependencies defined inside the package.json file
 *  as `dependencies` (NOT `devDependencies`!).
 *  This list will only be created once and on demand.
 *  Each dependency is represented by an object that contains the name,
 *  the main style and main script file, if available.
 *  For example:
 *  ```
 *    {
 *      name: 'dependencyName',
 *      script: 'index.js',
 *      style: 'src/index.scss'
 *    }
 *  ```
 */
const dependencies = getDependencies();

/**
 *  Helper method for creating absolute path from the root directory
 *  `__dirname/{root}/{glob}`
 */
const pathToRoot = (glob = '') => {
  return path.join(__dirname, root, glob);
};

/**
 *  Helper method for resolving paths to the app
 *  `app/{glob}`
 */
const pathToApp = (glob = '') => {
  return path.join(__dirname, root, 'app', glob);
};

/**
 *  Helper method for resolving paths to the apps components
 *  `app/components/{glob}`
 */
const pathToComponents = (glob = '') => {
  return path.join(__dirname, root, 'app/components', glob);
};

/**
 *  The main paths of your project handle these with care
 */
const paths = {
  js: pathToComponents('**/*!(.spec.js).js'), // exclude spec files
  scss: pathToApp('**/*.scss'), // stylesheets
  html: [
    pathToApp('**/*.html'),
    path.join(root, 'index.html')
  ],
  entry: [
    'babel-polyfill',
    pathToRoot('app/bootstrap/bootstrap.js')
  ],
  output: root,
  blankTemplates: 'generator/component/**/*.**',
  tmp: pathToRoot('.tmp'),
  dest: pathToRoot('dist'),
  mock: 'mock',

  // generator paths
  generatorTemplates: {
    component: 'generator/component/**/*.**'
  },

  // webpack paths
  webpack: {
    rootDir: path.join(__dirname, '..'),
    nodeModulesDir: path.join(__dirname, root, 'node_modules')
  }

};

/**
 *  List of available staging environments.
 */
exports.stages = {
  dev: 'dev',
  testing: 'testing'
};

exports.customEnv = {
  documentLanguages: ['en', 'de']
};

/**
 *  List of all tenants and their configuration.
 */
exports.tenants = readJson(pathToRoot('../tenants.json'));

exports.env = env;
exports.port = port;
exports.root = root;
exports.paths = paths;
exports.dependencies = dependencies;
exports.pathToApp = pathToApp;
exports.pathToComponents = pathToComponents;
