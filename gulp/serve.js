import gulp from 'gulp';
import path from 'path';
import webpack from 'webpack';
import serve from 'browser-sync';
import serveSpa from 'browser-sync-spa';
import historyApiFallback from 'connect-history-api-fallback';
import webpackDevMiddelware from 'webpack-dev-middleware';
import webpachHotMiddelware from 'webpack-hot-middleware';
import colorsSupported from 'supports-color';
import conf from './conf';
import webpackConfig from '@fino/ng1-webpack';

// Only needed for angular apps (single page applications)
serve.use(serveSpa({
  selector: '[ng-app]'
}));

gulp.task('serve', ['env', 'modules'], () => {
  const config = webpackConfig.dev(conf);

  config.entry.app = [
    // this modules required to make HRM working
    // it responsible for all this webpack magic
    'webpack-hot-middleware/client?reload=true'
    // application entry point
  ].concat(conf.paths.entry);

  let compiler = webpack(config);

  var proxy = require('./proxy')({
    context: '/api',
    secure: false,
    changeOrigin: true,
    port: process.env.api_port,
    host: process.env.api_host,
    logLevel: 'debug'
  });

  serve({
    port: conf.port,
    open: false,
    startPath: '/',
    server: {
      baseDir: path.join(__dirname, conf.root)
    },
    middleware: [
      proxy,
      historyApiFallback(),
      webpackDevMiddelware(compiler, {
        stats: {
          colors: colorsSupported,
          chunks: false,
          modules: false
        },
        publicPath: config.output.publicPath
      }),
      webpachHotMiddelware(compiler)
    ]
  });
});

gulp.task('serve:mock', ['mock', 'serve']);

gulp.task('watch', ['serve']);
